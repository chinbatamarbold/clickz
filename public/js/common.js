$('.with-pagination').append('<div class="swiper-pagination"></div>');
$('.with-pagination-bullets').append('<div class="swiper-pagination"></div>');
$('.with-navigation').append('<div class="swiper-button-next"></div><div class="swiper-button-prev"></div>');


$(document).ready(function(){
     $('.card-content>h5').each(function(i,element){
          if($(element).text().length>36){
               var short = $(element).text().substring(0,30);
               $(element).html(short+'...');
          }
     });
     $('.card-content>p').each(function(i,element){
          if($(element).text().length>50){
               var short = $(element).text().substring(0,50);
               $(element).html(short+'...');
          }

     });
});
$(document).ready(function(){
     $('.c-card__body>h2').each(function(i,element){
          if($(element).text().length>100){
               var short = $(element).text().substring(0,100);
               $(element).html(short+'...');
          }
     });
     $('.c-card__body>p').each(function(i,element){
          if($(element).text().length>145){
               var short = $(element).text().substring(0,120);
               $(element).html(short+'...');
          }

     });
});
// 비밀번호 보이기/숨기기

$('input[type=password]').on('keyup',function(){
     var length = $(this).val().length;
     var toggle_pass = $(this).next('.show-pass');
     if (length > 0) {
          toggle_pass.addClass('show');
     }else {
          toggle_pass.removeClass('show');
     }
});
$('.show-pass').on('click',function(){
     $(this).toggleClass('hide');
     var prev_input = $(this).prev('input');
     if (prev_input.attr('type') == "password") {
          prev_input.attr('type','text')
     }else {
          prev_input.attr('type','password')
     }
});

var swiper = new Swiper('.with-pagination', {
    loop: true,
    pagination: {
        el: '.swiper-pagination',
    },
});
var swiper = new Swiper('.with-navigation', {
    loop: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});
var swiper = new Swiper('.with-pagination-bullets', {
    loop: true,
    pagination: {
        el: '.swiper-pagination',
        dynamicBullets: true,
    },
});

var swiper = new Swiper('.with-pagination', {
    loop: true,
    pagination: {
       el: '.swiper-pagination',
    },
});
var swiper = new Swiper('.with-navigation', {
    loop: true,
    navigation: {
       nextEl: '.swiper-button-next',
       prevEl: '.swiper-button-prev',
    },
});
var swiper = new Swiper('.with-pagination-bullets', {
    loop: true,
    pagination: {
       el: '.swiper-pagination',
       dynamicBullets: true,
    },
});
var swiper = new Swiper('.partnerswipe', {
   slidesPerView: 6,
   spaceBetween: 20,
   loop: true,
   navigation: {
     nextEl: '.partner-button-next',
     prevEl: '.partner-button-prev',
   },
   autoplay: {
        delay: 2000,
        disableOnInteraction: false,
   }
 });
 var swiper = new Swiper('.partnerswipe', {
  // Default parameters
  slidesPerView: 6,
  spaceBetween: 20,
  // Responsive breakpoints
  breakpoints: {
    // when window width is <= 320px
    576: {
      slidesPerView: 2,
      spaceBetween: 10,
    },
    // when window width is <= 480px
    767: {
      slidesPerView: 3,
      spaceBetween: 20,
    },
    // when window width is <= 640px
    992: {
      slidesPerView: 4,
      spaceBetween: 20
    },
    1200: {
      slidesPerView: 6,
      spaceBetween: 20
    }
  }
});

$(window).scroll(function() {
    var height = $(window).scrollTop();
    if (height > 100) {
       $('#back2Top').fadeIn();
       $('.navbar').addClass('on');
    } else {
       $('#back2Top').fadeOut();
       $('.navbar').removeClass('on');
    }
});
$(document).ready(function() {
    $("#back2Top").click(function(event) {
       event.preventDefault();
       $("html, body").animate({ scrollTop: 0 }, "slow");
       return false;
    });
});

// // Baaska modal js oruulav
//
// var modal = document.getElementById("myModal");
//
// // Get the button that opens the modal
// var btn = document.getElementById("myBtn");
//
// // Get the <span> element that closes the modal
// var span = document.getElementsByClassName("close")[0];
//
// // When the user clicks on the button, open the modal
// btn.on('click',function(){
//      $('#myModal').show();
// });
//
// // When the user clicks on <span> (x), close the modal
// span.onclick = function() {
//      modal.style.display = "none";
// }
//
// // When the user clicks anywhere outside of the modal, close it
// window.onclick = function(event) {
//      if (event.target == modal) {
//           modal.style.display = "none";
//      }
// }
